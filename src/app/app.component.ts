import {Component} from '@angular/core';
import {Endereco} from "prgomesr-address";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  endereco = {} as Endereco;

  getEndereco(event: Endereco) {
    this.endereco = event;
  }
}
